# OpenML dataset: S2

https://www.openml.org/d/42111

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Synthetic 2-d data with N=5000 vectors and k=15 Gaussian clusters with different degree of cluster overlap 

P. Fr&auml;nti and O. Virmajoki, &quot;Iterative shrinking method for clustering problems&quot;, Pattern Recognition, 39 (5), 761-765, May 2006.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42111) of an [OpenML dataset](https://www.openml.org/d/42111). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42111/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42111/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42111/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

